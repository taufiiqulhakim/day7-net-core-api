﻿using MimeKit.Text;
using MimeKit;
using Org.BouncyCastle.Asn1.Ocsp;
using Crud.Model;
using MailKit.Net.Smtp;
using MailKit.Security;
using static Org.BouncyCastle.Math.EC.ECCurve;


namespace Crud.Services.EmailService
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _config;
        public EmailService(IConfiguration config)
        {
            _config = config;
        }

        public void SendEmail(EmailDTO request)
        {

            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse("rex.lockman13@ethereal.email\r\n"));
            email.To.Add(MailboxAddress.Parse(request.emailTo));
            //email.Subject = request.Subject;
            //email.Body = new TextPart(TextFormat.Html) { Text = request.Body };

            string txt = "<center><h1>Daily Notes</h1></center><ul>";

            if (request.Notes != null)
            {
                foreach (string dnm in request.Notes)
                {
                    string temp = "<li>" + dnm + "</li>";
                    txt += temp;
                }
            }
            txt += "</ul>";

            email.Body = new TextPart(TextFormat.Html) { Text = txt };

            var List = request.Notes;

            using var smtp = new SmtpClient();
            smtp.Connect(_config.GetSection("EmailHost").Value, Convert.ToInt32(_config.GetSection("EmailPort").Value), useSsl: true);
            smtp.Authenticate(_config.GetSection("EmailUserName").Value, _config.GetSection("EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);



        }
    }

}

