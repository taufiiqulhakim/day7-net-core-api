﻿using Crud.Model;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;


namespace Crud.Services.EmailService
{
    public interface IEmailService
    {
        void SendEmail(EmailDTO request);

    }
}
