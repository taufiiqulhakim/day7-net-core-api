﻿using Crud.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Crud.controllers
{
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public TasksController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [Authorize]
        [HttpGet]
        [Route("api/tasks/GetUserWithTask")]


        public List<OutputModel> GetUserWithTask(string? name)
        {
            List<OutputModel> output = new List<OutputModel>();

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string queryUser = "Select * from Users";
                SqlCommand cmd = new SqlCommand(queryUser, conn);

                if (!string.IsNullOrEmpty(name))
                {
                    queryUser = "Select * from Users where name= '@name'";
                    cmd = new SqlCommand(queryUser, conn);
                    cmd.Parameters.AddWithValue("@name", name);
                }
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataTable dtUser = new DataTable();
                sqlDataAdapter.Fill(dtUser);

                string queryTasks = "Select * from tasks";
                SqlCommand cmdTasks = new SqlCommand(queryTasks, conn);
                sqlDataAdapter = new SqlDataAdapter(cmdTasks);
                DataTable dtTasks = new DataTable();
                sqlDataAdapter.Fill(dtTasks);

                for (int i = 0; i < dtUser.Rows.Count; i++)
                {
                    OutputModel oModel = new OutputModel();
                    oModel.pk_users_id = Convert.ToInt32(dtUser.Rows[i]["pk_users_id"].ToString());
                    oModel.name = dtUser.Rows[i]["name"].ToString();
                    List<TaskModel> listTasks = new List<TaskModel>();

                    for (int j = 0; j < dtTasks.Rows.Count; j++)
                    {
                        if (dtUser.Rows[i]["pk_users_id"].ToString() == dtTasks.Rows[j]["fk_users_id"].ToString())
                        {
                            TaskModel tModel = new TaskModel();
                            tModel.pk_tasks_id = Convert.ToInt32(dtTasks.Rows[j]["pk_tasks_id"].ToString());
                            tModel.task_detail = dtTasks.Rows[j]["task_detail"].ToString();
                            listTasks.Add(tModel);
                        }
                    }
                    oModel.tasks = listTasks;
                    output.Add(oModel);
                }
            }


            return output;
        }

        [HttpPost]
        [Route("api/tasks/AddUserWithTask")]
        public string PostUser([FromBody] OutputModel outputModel)
        {
            string userQuery = "INSERT INTO Users (name) VALUES (@Name)";

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                SqlCommand userCmd = new SqlCommand(userQuery, conn);
                userCmd.Parameters.AddWithValue("@Name", outputModel.name);
                conn.Open();
                userCmd.ExecuteNonQuery();
                conn.Close();
            }

            foreach (var task in outputModel.tasks)
            {
                string taskQuery = "INSERT INTO Tasks (task_detail, fk_users_id) VALUES (@TaskDetail, (SELECT IDENT_CURRENT('Users')))";
                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    SqlCommand taskCmd = new SqlCommand(taskQuery, conn);
                    taskCmd.Parameters.AddWithValue("@TaskDetail", task.task_detail);
                    conn.Open();
                    taskCmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            return "User added successfully";
        }

        [HttpGet]
        [Route("api/tasks/GetUserWithTaskByName")]
        public List<OutputModel> GetUserWithTaskByName(string? name)
        {
            List<OutputModel> userList = new List<OutputModel>();
            string query = "SELECT u.pk_users_id, u.name, t.pk_tasks_id, t.task_detail, t.fk_users_id FROM users u JOIN tasks t ON u.pk_users_id = t.fk_users_id";

            // Check if the 'name' parameter is provided and add the WHERE clause if necessary
            if (!string.IsNullOrEmpty(name))
            {
                query += " WHERE u.name = @name";
            }

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                SqlCommand cmdTasks = new SqlCommand(query, conn);

                // Add the parameter if 'name' is not null or empty
                if (!string.IsNullOrEmpty(name))
                {
                    cmdTasks.Parameters.AddWithValue("@name", name);
                }

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdTasks);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    int userId = Convert.ToInt32(row["pk_users_id"]);
                    string userName = row["name"].ToString();

                    OutputModel user = userList.FirstOrDefault(u => u.pk_users_id == userId);
                    if (user == null)
                    {
                        user = new OutputModel
                        {
                            pk_users_id = userId,
                            name = userName
                        };
                        userList.Add(user);
                    }

                    TaskModel task = new TaskModel
                    {
                        pk_tasks_id = Convert.ToInt32(row["pk_tasks_id"]),
                        task_detail = row["task_detail"].ToString(),
                        fk_users_id = Convert.ToInt32(row["fk_users_id"])
                    };

                    user.tasks.Add(task);
                }
            }
            return userList;
        }


    }
}
                

         

