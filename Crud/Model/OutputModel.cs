﻿namespace Crud.model
{
    public class OutputModel
    {
        public int? pk_users_id { get; set; }
        public string? name { get; set; }

        public List<TaskModel>? tasks { get; set; }

        public OutputModel()
        {
            tasks = new List<TaskModel>();
        }

    }
}
